import React from "react";
import { firestore } from "./firestore";
import Assignment from "./Assignment";

class Assignments extends React.Component {
  state = {
    assignments: []
  };

  componentDidMount() {
    console.log("Loading firestore data");
    this.getData();
  }

  getData = () => {
    const db = firestore.firestore();

    db.collection("assignments")
      .get()
      .then(snapshot => {
        var wholeData = [];
        snapshot.forEach(doc => {
          console.log(doc.data());
          wholeData.push(doc.data());
        });
        this.setState({ assignments: wholeData });
      })
      .catch(error => {
        console.log("Error!", error);
      });
    db.collection("assignments").onSnapshot(this.listenToChange);
  };

  listenToChange = e => {
    var wholeData = [];
    e.forEach(function(doc) {
      wholeData.push(doc.data());
    });
    this.setState({ assignments: wholeData });
  };

  render() {
    return (
      <div>
        {this.state.assignments.map(function(assignment, i) {
          return <Assignment assignment={assignment} key={i} />;
        })}
      </div>
    );
  }
}

export default Assignments;
