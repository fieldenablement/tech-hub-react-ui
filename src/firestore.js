import firebase from "firebase";

export const firestore = firebase.initializeApp({
  apiKey: "AIzaSyCuPfyXIERvRBJQKNqau90gFCcLsnj6rO4",
  authDomain: "techhubpoc.firebaseapp.com",
  databaseURL: "https://techhubpoc.firebaseio.com",
  projectId: "techhubpoc",
  storageBucket: "techhubpoc.appspot.com",
  messagingSenderId: "98143587194"
});
