import React, { Component } from "react";
import "./App.css";
import Assignments from "./Assignments";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Assignments />
        </header>
      </div>
    );
  }
}

export default App;
