import React from "react";
import axios from "axios";
import { firestore } from "./firestore";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import CircularProgress from "@material-ui/core/CircularProgress";

import "./Assignment.css";

class Assignment extends React.Component {
  state = {
    customerName: "Unnkown",
    isLoadingCustomerName: false
  };

  accept = () => {
    console.log("Accept is clicked!");
    const db = firestore.firestore();
    var collection = db.collection("assignments");
    var document = collection.doc(this.props.assignment.assigmentId);
    var newRequestDocument = db
      .collection("messages")
      .doc(this.props.assignment.assigmentId);
    var newRequest = {
      event: "TECH_ACCEPT_JOB",
      category: "TECH_UPDATE_JOB_STATUS",
      from: "TECHNICIAN",
      to: "DISPATCHER",
      assigmentId: this.props.assignment.assigmentId,
      jobId: this.props.assignment.jobId,
      teamWorkerId: this.props.assignment.teamWorkerId,
      statusCd: "ACCEPTED"
    };
    return db.runTransaction(function(transaction) {
      return transaction.get(document).then(function(doc) {
        transaction.update(document, {
          statusCd: "ACCEPTED"
        });
        return transaction.set(newRequestDocument, newRequest);
      });
    });
  };

  getCustomerName = () => {
    console.log("retrieving customername");
    this.setState({ isLoadingCustomerName: true });
    axios
      .get(
        `https://instant-river-235304.appspot.com/assignment/` +
          this.props.assignment.assigmentId
      )
      .then(res => {
        console.log(res.data);
        this.setState({
          customerName: res.data.name,
          isLoadingCustomerName: false
        });
      });
  };

  render() {
    const isLoadingCustomerName = this.state.isLoadingCustomerName;
    let customer;
    if (isLoadingCustomerName) {
      customer = (
        <Typography>
          Customer Name: <CircularProgress size={14} />
        </Typography>
      );
    } else {
      customer = (
        <Typography>Customer Name: {this.state.customerName}</Typography>
      );
    }
    return (
      <Card>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Assignment Id :{this.props.assignment.assigmentId}
          </Typography>
          <Typography component="p">
            WorkOrder Id : {this.props.assignment.workOrderId}
          </Typography>
          <Typography>
            {this.props.assignment.classificationCd},{" "}
            {this.props.assignment.productCategoryCd},{" "}
            {this.props.assignment.jobTypeCd},{" "}
            {this.props.assignment.serviceClassCd}
          </Typography>
          <Typography>
            Location: {this.props.assignment.locationAddressTxt}
          </Typography>
          {customer}
          <Typography component="p">
            Job Status: {this.props.assignment.statusCd}{" "}
          </Typography>
          <CardActions>
            <Button onClick={() => this.accept()} size="small" color="primary">
              Accept via Firebase
            </Button>
            <br />

            <Button
              onClick={() => this.getCustomerName()}
              size="small"
              color="primary"
            >
              Get Customer Name via Web Service
            </Button>
          </CardActions>
        </CardContent>
      </Card>
    );
  }
}

export default Assignment;
